#!/usr/bin/python3

import os 
import sys


if __name__ == "__main__":
    filename = sys.argv[1]
    ip = sys.argv[2]
    difficulty = sys.argv[3]
    machine = sys.argv[4]


    if len(sys.argv) == 1 and len(sys.argv) > 4:
        sys.stderr.write(str(sys.argv[0]) + "name ip difficulty system")

    full_path = "/home/adrien/Desktop/" + filename
    if os.path.exists(full_path):
        sys.stderr.write("Le dossier existe déjà !")
        exit(1)
    else:
        os.makedirs(full_path)
        os.makedirs(full_path + '/recon')
        notes = open(full_path + "/Notes.md","w")

        notes.write(f"#\t{filename}\n")
        notes.write(f"*\tIP -> {ip}\n")
        notes.write(f"*\tDifficulty -> {difficulty}\n")
        notes.write(f"*\tSystem -> {machine}\n\n")
        notes.write(f"##\tRecon\n\n")

        notes.close()

